# max.model.ledger.simplefabric

Simple and generic Hyperledger Fabric model that allows adversarial attacks.

It substitutes [max.model.ledger.hyperledger-fabric](https://gitlab.com/cea-licia/max/models/ledgers/max.model.ledger.hyperledger-fabric)
and is based on [max.model.ledger.abstract_ledger](https://gitlab.com/cea-licia/max/models/ledgers/max.model.ledger.abstract_ledger) 
and [max.model.network.stochastic_adversarial_p2p](https://gitlab.com/cea-licia/max/models/networks/max.model.network.stochastic_adversarial_p2p) rather than on
[max.model.ledger.blockchain](https://gitlab.com/cea-licia/max/models/ledgers/max.model.ledger.blockchain)
and [max.model.network.p2p](https://gitlab.com/cea-licia/max/models/networks/max.model.network.p2p).

The genericity of this model makes so that we can use:
- any application-layer usecase : the nature of the transactions T_tx and the local ledger states T_st remain generic, following the conventions in [max.model.ledger.abstract_ledger](https://gitlab.com/cea-licia/max/models/ledgers/max.model.ledger.abstract_ledger)
- any ledger implementation for the ordering service : we can use any model that derives from [max.model.ledger.abstract_ledger](https://gitlab.com/cea-licia/max/models/ledgers/max.model.ledger.abstract_ledger) .

It is an advantage over [max.model.ledger.hyperledger-fabric](https://gitlab.com/cea-licia/max/models/ledgers/max.model.ledger.hyperledger-fabric)
which:
- only considered a specific usecase 
(see e.g. [the measurements that are exchanged](https://gitlab.com/cea-licia/max/models/ledgers/max.model.ledger.hyperledger-fabric/-/blob/main/src/main/java/max/model/ledger/hyperledgerfabric/Measurement.java?ref_type=heads)
or [the declaration of specific applications A and B](https://gitlab.com/cea-licia/max/models/ledgers/max.model.ledger.hyperledger-fabric/-/blob/main/src/main/java/max/model/ledger/hyperledgerfabric/attack/action/ACCensorAppAUsingOrderer.java?ref_type=heads)
)
- explicitly relied on [max.model.ledger.tendermint_v2](https://gitlab.com/cea-licia/max/models/ledgers/max.model.ledger.tendermint_v2) for the ordering layer whereas we may use any algorithm.



## Application to a mockup usecase and verification of fairness properties

We apply the same usecase developed in [max.model.ledger.abstract_ledger](https://gitlab.com/cea-licia/max/models/ledgers/max.model.ledger.abstract_ledger) and
[max.model.ledger.simpleming](https://gitlab.com/cea-licia/max/models/ledgers/max.model.ledger.simpleming).

We use a Tendermint model for the ordering service (for this specific usecase) and transactions correspond to clients submitting solutions to puzzles.

The [FabricPuzzleSolvingTest](https://gitlab.com/cea-licia/max/models/ledgers/max.model.ledger.simplefabric/-/blob/main/src/test/java/max/model/ledger/simplefabric/env/FabricPuzzleSolvingTest.java?ref_type=heads)
class parameterizes a simple simulation in which a number of applications, peers and orderers treat the submission of puzzle solutions by a number of clients.


### Initial steps of the simulation

We consider a simple example with only one HF application which forwards transactions from the clients.
We consider a network with 10 orderers and 10 peers.
Hence the network contains 21 distinct agents.

#### Environment and Application 

At the start of the simulation, the Fabric Environment and the Tendermint Environment (which is used for the ordering service)
are setup:

<img src="./README_images/setup/setup_env1.png" height="120">
<img src="./README_images/setup/setup_env2.png" height="100">

The application is also created and its message handler configured
(the application collects endorsements from peers and, once a threshold is reached, 
broadcasts the endorsed transaction to the ordering service):

<img src="./README_images/setup/setup_app1.png" height="100">
<img src="./README_images/setup/setup_app2.png" height="60">

#### Peers

Similarly, the peers are setup in two phases:

A first phase in which roles are set up and the peers are activated:

<img src="./README_images/setup/setup_peers1.png" height="100">

A second phase in which their message handlers are configured:

<img src="./README_images/setup/setup_peers2.png" height="60">

#### Orderers

Similarly, the orderers are setup in two phases:

A first phase in which roles are set up and the orderers are activated:

<img src="./README_images/setup/setup_orderers1.png" height="180">

A second phase in which their message handlers, 
local ledger states, 
transactions approval policies and byzantine threshold are configured:

<img src="./README_images/setup/setup_orderers2.png" height="260">


### Execution of the behavior

Clients (simulated by the environment) start sending their transactions to the application which then broadcast these
transactions to all the peers of the endorsing service (in order to then collect their endorsements).

<img src="./README_images/exec/first_puzzle_solution.png" height="160">

In the meanwhile, the Tendermint protocol also starts, with a first height, a first proposer, and
PROPOSE, PREVOTE and PRECOMMIT messages being exchanged (empty blocks being authorized).

<img src="./README_images/exec/first_height.png" height="120">

<img src="./README_images/exec/first_proposer.png" height="240">

Empty blocks being authorized, towards the beginning of the simulation, the first few blocks are empty.

<img src="./README_images/exec/first_block.png" height="160">


The peers start receiving endorsement requests and reply to the application :

<img src="./README_images/exec/first_endorsement.png" height="140">


Once the application collect enough endorsements for a specific transaction, 
the endorsed transaction is broadcast to the orderers :

<img src="./README_images/exec/first_endorsed_tr_to_ordering.png" height="180">


The orderers now having access to content to put in blocks, new blocks start containing transactions with puzzle solutions,
which allows determining the winners of puzzles :

<img src="./README_images/exec/first_block_with_solutions.png" height="560">


### Verification of properties during or after the simulation

At runtime, we can verify various properties.

Given a snapshot in time, we can verify the coherence of the ledger i.e. if for every pair of orderer,
their local copy of the blockchain are such that one is a prefix of the other :

<img src="./README_images/check/check_coherence.png" height="240">

Also, we can count the number of duplicated transactions that are delivered (here none) :

<img src="./README_images/check/check_duplicated.png" height="60">

Also, we can also count the number of messages that have transited up to this point of the simulation.
The messages that are counted are characterized by the environment in which they were exchanged
and a certain filter on their nature (i.e. what kind of message).
Because there is no such thing as "counting messages", it is the number of distinct emission and reception events
that we count:

<img src="./README_images/check/check_msgcount.png" height="160">

We can also evaluate some fairness properties w.r.t.:
- the likelihood for each client to win (a form of "client-fairness")
- various "order-fairness" properties on the ordering service i.e.
relating the order with which transactions are eventually delivered and 
the order with which endorsed transactions are received by the orderers
- various "order-fairness" properties on the endorsing service i.e.
  relating the order with which transactions are eventually delivered and
  the order with which transactions are initially received by the peers

<img src="./README_images/check/check_fairness.png" height="300">


## Logs of input and output events

Via the API from  [max.model.network.stochastic_adversarial_p2p](https://gitlab.com/cea-licia/max/models/networks/max.model.network.stochastic_adversarial_p2p),
it is also possible to generate log files that only relate to the atomic message input and output events
that occur on each node.

For instance, below, for a simulation with 7=(3*2)+1 orderers (using Tendermint), 
4 peers (given two organizations each having 2 peers) 
and 1 application, 
we have 12 log files, each corresponding to a sequence of input and output event observed locally on one of
the nodes.

<img src="./README_images/logsfabric.png"> 


## Implementation of attacks

Because this model is built on top of [max.model.network.stochastic_adversarial_p2p](https://gitlab.com/cea-licia/max/models/networks/max.model.network.stochastic_adversarial_p2p),
we can use its adversarial model.

The behavior of all peers and orderers being defined purely locally (no global oracle in contrast to [max.model.ledger.tendermint_v2](https://gitlab.com/cea-licia/max/models/ledgers/max.model.ledger.tendermint_v2))
, an adversary can attack individual nodes to modify their behaviors.
This can be done e.g. via:
- "AcSetLocalApprovalPolicy" from [max.model.ledger.abstract_ledger](https://gitlab.com/cea-licia/max/models/ledgers/max.model.ledger.abstract_ledger) to e.g., prevent Orderers to propose blocks/vertices that contain specific transactions
- "[AcSetFabricPeerTxApprovalPolicy](https://gitlab.com/cea-licia/max/models/ledgers/max.model.ledger.simplefabric/-/blob/main/src/main/java/max/model/ledger/simplefabric/action/AcSetFabricPeerTxApprovalPolicy.java?ref_type=heads)" to e.g., prevent Peers to endorse specific transactions
- "AcAddCommunicationCensor" from [max.model.network.stochastic_adversarial_p2p](https://gitlab.com/cea-licia/max/models/networks/max.model.network.stochastic_adversarial_p2p)
- "AcAddCommunicationDelay" from [max.model.network.stochastic_adversarial_p2p](https://gitlab.com/cea-licia/max/models/networks/max.model.network.stochastic_adversarial_p2p) to add communication delays
- "AcAddCommunicationRedirect" from [max.model.network.stochastic_adversarial_p2p](https://gitlab.com/cea-licia/max/models/networks/max.model.network.stochastic_adversarial_p2p)
- "AcConfigMessageHandler" from [max.model.network.stochastic_adversarial_p2p](https://gitlab.com/cea-licia/max/models/networks/max.model.network.stochastic_adversarial_p2p) which can be used to change the manner with
which agents react to messages


# Licence

__max.model.ledger.simplefabric__ is released under
the [Eclipse Public Licence 2.0 (EPL 2.0)](https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html)













