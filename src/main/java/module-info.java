open module max.model.ledger.simplefabric {
    // Exported packages
    exports max.model.ledger.simplefabric.action;
    exports max.model.ledger.simplefabric.action.comms;
    exports max.model.ledger.simplefabric.agent;
    exports max.model.ledger.simplefabric.agent.peer_approval;
    exports max.model.ledger.simplefabric.agent.endorsement_policy;
    exports max.model.ledger.simplefabric.env;
    exports max.model.ledger.simplefabric.message;
    exports max.model.ledger.simplefabric.message.handler;
    exports max.model.ledger.simplefabric.message.payload;
    exports max.model.ledger.simplefabric.peer_state;
    exports max.model.ledger.simplefabric.role;
    exports max.model.ledger.simplefabric.usecase.puzzle;

    // Dependencies
    requires java.logging;
    requires java.desktop;
    requires org.apache.commons.lang3;

    requires madkit;      // Automatic

    requires max.core;
    requires max.datatype.com;
    requires max.model.network.stochastic_adversarial_p2p;
    requires max.model.ledger.abstract_ledger;

    // Optional dependency (required for tests)
    requires static org.junit.jupiter.api;

}