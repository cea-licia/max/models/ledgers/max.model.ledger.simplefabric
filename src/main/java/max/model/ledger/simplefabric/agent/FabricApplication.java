/*********************************************************************
 * Copyright (c) 2023 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/

package max.model.ledger.simplefabric.agent;

import max.core.action.Plan;
import max.model.ledger.simplefabric.agent.endorsement_policy.AbstractFabricEndorsementPolicy;
import max.model.ledger.simplefabric.role.RFabricApplication;
import max.model.network.stochastic_adversarial_p2p.agent.StochasticPeerAgent;


/**
 * Base class for HyperLedger Fabric Applications.
 *
 * @author Erwan Mahe
 */
public class FabricApplication<T_tx> extends StochasticPeerAgent {


    /**
     * For Applications to keep track of endorsements replied by Peers.
     * And of the thresholds of endorsements required to forward a transaction to the ordering service.
     * **/
    public AbstractFabricEndorsementPolicy<T_tx> endorsementPolicy;

    public FabricApplication(Plan<? extends FabricApplication> plan, AbstractFabricEndorsementPolicy<T_tx> endorsementPolicy) {
        super(plan);
        addPlayableRole(RFabricApplication.class);
        this.endorsementPolicy = endorsementPolicy;
    }

}
