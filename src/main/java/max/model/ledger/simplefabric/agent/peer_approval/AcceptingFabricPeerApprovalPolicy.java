/*********************************************************************
 * Copyright (c) 2023 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/

package max.model.ledger.simplefabric.agent.peer_approval;



/**
 * Always approve transactions.
 *
 * @author Erwan Mahe
 */
public class AcceptingFabricPeerApprovalPolicy<T_tx> implements AbstractFabricPeerApprovalPolicy<T_tx> {

    @Override
    public boolean approve(T_tx transactionToApprove) {
        return true;
    }

    @Override
    public AbstractFabricPeerApprovalPolicy<T_tx> copy_as_new() {
        return new AcceptingFabricPeerApprovalPolicy();
    }

}
