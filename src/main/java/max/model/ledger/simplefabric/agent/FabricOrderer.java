/*********************************************************************
 * Copyright (c) 2023 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/

package max.model.ledger.simplefabric.agent;

import max.core.action.Plan;
import max.core.role.Role;
import max.model.ledger.simplefabric.agent.endorsement_policy.AbstractFabricEndorsementPolicy;
import max.model.ledger.simplefabric.role.RFabricOrderer;
import max.model.network.stochastic_adversarial_p2p.agent.StochasticPeerAgent;



/**
 * Base class for HyperLedger Fabric Orderers.
 *
 * @author Erwan Mahe
 */
public class FabricOrderer<T_tx> extends StochasticPeerAgent {

    /**
     * For Orderers to be able to verify that a transaction forwarded by an application has indeed
     * enough endorsements by peers to be ordered.
     * **/
    public AbstractFabricEndorsementPolicy<T_tx> endorsementPolicy;

    public FabricOrderer(Plan<? extends FabricOrderer> plan, AbstractFabricEndorsementPolicy<T_tx> endorsementPolicy) {
        super(plan);
        this.addPlayableRole(RFabricOrderer.class);
        this.endorsementPolicy = endorsementPolicy;
    }

    public void addLedgerValidatorRole(Class<? extends Role> validatorRole) {
        this.addPlayableRole(validatorRole);
    }

}
