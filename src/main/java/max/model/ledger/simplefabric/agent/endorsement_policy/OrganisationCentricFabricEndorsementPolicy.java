/*********************************************************************
 * Copyright (c) 2023 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/

package max.model.ledger.simplefabric.agent.endorsement_policy;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Always approve transactions.
 *
 * @author Erwan Mahe
 */
public class OrganisationCentricFabricEndorsementPolicy<T_tx> implements AbstractFabricEndorsementPolicy<T_tx> {

    /**
     * Of which organisation each peer is part of.
     * **/
    private HashMap<String,String> peerToOrganizationMap;

    /**
     * Required number of endorsement per distinct organization.
     * **/
    private HashMap<String,Integer> thresholdPerOrganization;

    /**
     * Keeps track of each individual endorsement.
     * **/
    private HashMap<T_tx, HashSet<String>> endorsements;

    public OrganisationCentricFabricEndorsementPolicy(HashMap<String,String> peerToOrganizationMap,
                                                      HashMap<String,Integer> thresholdPerOrganization) {
        this.peerToOrganizationMap = peerToOrganizationMap;
        this.thresholdPerOrganization = thresholdPerOrganization;
        this.endorsements = new HashMap<>();
    }

    @Override
    public void initiate_transaction(T_tx transaction) {
        this.endorsements.put(transaction,new HashSet<>());
    }

    @Override
    public boolean register_endorsement(T_tx endorsedTransaction, String endorsedBy) {
        if (this.endorsements.containsKey(endorsedTransaction)) {
            HashSet<String> by = this.endorsements.get(endorsedTransaction);
            by.add(endorsedBy);
            this.endorsements.put(endorsedTransaction,by);
            return true;
        } else {
            return false;
        }
    }

    @Override
    public List<String> get_endorsed_by(T_tx transaction) {
        return this.endorsements.get(transaction).stream().sorted().collect(Collectors.toList());
    }

    @Override
    public void forget_transaction(T_tx transaction) {
        this.endorsements.remove(transaction);
    }

    @Override
    public boolean check_endorsement_threshold_reached(T_tx endorsedTransaction) {
        return this.verify_endorsements_from_app(endorsedTransaction, this.endorsements.get(endorsedTransaction));
    }

    @Override
    public boolean verify_endorsements_from_app(T_tx endorsedTransaction, HashSet<String> endorsedBy) {
        // collects the number of endorsements per organization
        Map<String,Integer> endorsementsPerOrganization = this.thresholdPerOrganization.keySet().stream().collect(
                Collectors.toMap(key -> key, key -> 0)
        );
        for (String by : endorsedBy) {
            String organizationOfPeer = this.peerToOrganizationMap.get(by);
            Integer num = endorsementsPerOrganization.get(organizationOfPeer);
            endorsementsPerOrganization.put(organizationOfPeer, num+1);
        }
        // compare it with the required threshold
        for (String organization : this.thresholdPerOrganization.keySet()) {
            Integer requiredNumber = this.thresholdPerOrganization.get(organization);
            Integer effectiveNumber = endorsementsPerOrganization.get(organization);
            if (effectiveNumber < requiredNumber) {
                return false;
            }
        }
        return true;
    }

    @Override
    public AbstractFabricEndorsementPolicy<T_tx> copy_as_new() {
        HashMap<String,String> peerToOrganizationMapCopy = (HashMap<String,String>) this.peerToOrganizationMap.entrySet().stream()
                .collect(Collectors.toMap(e -> e.getKey(), e -> e.getValue()));
        HashMap<String,Integer> thresholdPerOrganizationCopy = (HashMap<String,Integer>) this.thresholdPerOrganization.entrySet().stream()
                .collect(Collectors.toMap(e -> e.getKey(), e -> e.getValue()));
        return new OrganisationCentricFabricEndorsementPolicy(
                peerToOrganizationMapCopy,
                thresholdPerOrganizationCopy
        );
    }

}
