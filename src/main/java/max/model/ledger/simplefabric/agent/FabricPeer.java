/*********************************************************************
 * Copyright (c) 2023 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/

package max.model.ledger.simplefabric.agent;

import max.core.action.Plan;
import max.model.ledger.simplefabric.agent.peer_approval.AbstractFabricPeerApprovalPolicy;
import max.model.ledger.simplefabric.peer_state.ApplicationSpecificPeerState;
import max.model.ledger.simplefabric.role.RFabricPeer;
import max.model.network.stochastic_adversarial_p2p.agent.StochasticPeerAgent;

import java.util.HashMap;
import java.util.HashSet;

/**
 * Base class for HF Peers.
 *
 * @author Erwan Mahe
 */
public class FabricPeer<T_tx> extends StochasticPeerAgent {

    /**
     * To keep track of emitted Endorsements, replying to specific applications.
     * **/
    public final HashMap<String, HashSet<T_tx>> repliedEndorsementsPerApplicationName;

    public AbstractFabricPeerApprovalPolicy<T_tx> fabricPeerApprovalPolicy;

    public ApplicationSpecificPeerState<T_tx> applicationSpecificPeerState;

    public FabricPeer(Plan<? extends FabricPeer> plan,
                      AbstractFabricPeerApprovalPolicy<T_tx> fabricPeerApprovalPolicy,
                      ApplicationSpecificPeerState<T_tx> applicationSpecificPeerState) {
        super(plan);
        addPlayableRole(RFabricPeer.class);
        this.repliedEndorsementsPerApplicationName = new HashMap<>();
        this.fabricPeerApprovalPolicy = fabricPeerApprovalPolicy;
        this.applicationSpecificPeerState = applicationSpecificPeerState;
    }

}
