/*********************************************************************
 * Copyright (c) 2023 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/

package max.model.ledger.simplefabric.agent.endorsement_policy;


import java.util.HashSet;
import java.util.List;

/**
 * Endorsement policy used by:
 * - applications to keep track of the endorsements they receive back from peers (i.e. via *FabricReplyEndorsementPayload*)
 * - orderers to check whether or not a *FabricEndorsedTransactionPayload* send by an application indeed has enough endorsements
 *
 * @author Erwan Mahe
 */
public interface AbstractFabricEndorsementPolicy<T_tx> {


    /**
     * Called by a *FabricApplication* to initiate keeping track of endorsements.
     * **/
    void initiate_transaction(T_tx transaction);

    /**
     * Called by a *FabricApplication* whenever it receives a *FabricReplyEndorsementPayload*
     * It updates its internal memory to keep track of the new endorsement.
     *
     * The boolean represents the success of the registration.
     * **/
    boolean register_endorsement(T_tx endorsedTransaction, String endorsedBy);


    /**
     * Called by a *FabricApplication* to get all peers that, according to its own knowledge, have endorsed the given transaction.
     * **/
    List<String> get_endorsed_by(T_tx transaction);

    /**
     * Called by a *FabricApplication* after having submitted an endorsed transaction.
     * **/
    void forget_transaction(T_tx transaction);


    /**
     * Called by a *FabricApplication* whenever it receives a *FabricReplyEndorsementPayload*
     * It verifies whether or not, with the newly registered endorsement, it has reached enough endorsements
     * to forward the transaction to the ordering service.
     * **/
    boolean check_endorsement_threshold_reached(T_tx endorsedTransaction);


    /**
     * Called by a *FabricOrdered* whenever it receives a *FabricEndorsedTransactionPayload*
     * **/
    boolean verify_endorsements_from_app(T_tx endorsedTransaction, HashSet<String> endorsedBy);

    AbstractFabricEndorsementPolicy<T_tx> copy_as_new();
}
