/*********************************************************************
 * Copyright (c) 2023 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/

package max.model.ledger.simplefabric.agent.peer_approval;



/**
 * An abstract and generic policy to approve transactions.
 * Here, it defines whether or not a peer will endorse a given transaction upon receiving it.
 *
 * @author Erwan Mahe
 */
public interface AbstractFabricPeerApprovalPolicy<T_tx> {
    boolean approve(T_tx transactionToApprove);

    AbstractFabricPeerApprovalPolicy<T_tx> copy_as_new();
}
