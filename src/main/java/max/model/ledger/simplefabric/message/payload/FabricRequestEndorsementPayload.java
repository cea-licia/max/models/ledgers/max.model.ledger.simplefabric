/*********************************************************************
 * Copyright (c) 2023 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/

package max.model.ledger.simplefabric.message.payload;


/**
 * Payload of Hyperledger Fabric messages that concern
 * requests to clients for transaction endorsement
 *
 * @author Erwan Mahe
 */
public class FabricRequestEndorsementPayload<T_tx> {

    public final T_tx transaction;

    public final String applicationName;

    public FabricRequestEndorsementPayload(T_tx transaction, String applicationName) {
        this.transaction = transaction;
        this.applicationName = applicationName;
    }

    @Override
    public int hashCode() {
        int result = this.transaction.hashCode();
        result = 31 * result + this.applicationName.hashCode();
        return result;
    }

}
