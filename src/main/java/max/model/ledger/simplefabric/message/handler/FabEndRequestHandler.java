/*********************************************************************
 * Copyright (c) 2023 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/

package max.model.ledger.simplefabric.message.handler;


import madkit.kernel.AgentAddress;
import max.datatype.com.Message;
import max.model.ledger.abstract_ledger.state.AbstractLocalLedgerState;
import max.model.ledger.simplefabric.action.comms.AcSendEndorsementReply;
import max.model.ledger.simplefabric.agent.FabricPeer;
import max.model.ledger.simplefabric.message.payload.FabricRequestEndorsementPayload;
import max.model.network.stochastic_adversarial_p2p.agent.StochasticPeerAgent;
import max.model.network.stochastic_adversarial_p2p.context.StochasticP2PContext;
import max.model.network.stochastic_adversarial_p2p.context.handler.OnReceiveMessageHandler;

import java.util.HashSet;


/**
 * Message handler that handles {@link max.model.ledger.simplefabric.message.FabricMessageType#RequestEndorsement} messages.
 *
 * @author Erwan Mahe
 */
public class FabEndRequestHandler<T_tx, T_st extends AbstractLocalLedgerState<T_tx>> implements OnReceiveMessageHandler {

    @Override
    public void handle(StochasticP2PContext context, Message<AgentAddress, ?> message) {
        // ***
        FabricPeer<T_tx> peer = (FabricPeer<T_tx>) context.getOwner();

        FabricRequestEndorsementPayload<T_tx> fabPayload = (FabricRequestEndorsementPayload<T_tx> ) message.getPayload();

        String peerName = peer.getName();
        if (!peer.repliedEndorsementsPerApplicationName.containsKey(fabPayload.applicationName)) {
            peer.repliedEndorsementsPerApplicationName.put(fabPayload.applicationName, new HashSet<>());
        }
        HashSet<T_tx> alreadyEndorsed = peer.repliedEndorsementsPerApplicationName.get(fabPayload.applicationName);
        if (alreadyEndorsed.contains(fabPayload.transaction)) {
            peer.getLogger().fine("Peer " + peerName + " already handled transaction");
        } else {
            peer.getLogger().info("Peer " + peerName + " acknowledging reception of transaction");
            peer.applicationSpecificPeerState.onReceiveEndorsementRequest(fabPayload.transaction);
            if (peer.fabricPeerApprovalPolicy.approve(fabPayload.transaction)) {
                peer.getLogger().info("Peer " + peerName + " approves transaction");
                (new AcSendEndorsementReply(
                        context.getEnvironmentName(),
                        (StochasticPeerAgent) context.getOwner(),
                        fabPayload.transaction,
                        fabPayload.applicationName)
                ).execute();
                alreadyEndorsed.add(fabPayload.transaction);
                peer.repliedEndorsementsPerApplicationName.put(fabPayload.applicationName,alreadyEndorsed);
            }
        }

    }

}
