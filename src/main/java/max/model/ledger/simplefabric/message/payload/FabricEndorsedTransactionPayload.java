/*********************************************************************
 * Copyright (c) 2023 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/

package max.model.ledger.simplefabric.message.payload;


import java.util.List;

/**
 * Payload of Hyperledger Fabric messages that concern
 * the submission of an endorsed transaction
 * by an application to the orderers of the ordering service
 *
 * @author Erwan Mahe
 */
public class FabricEndorsedTransactionPayload<T_tx> {

    public final T_tx transaction;

    public List<String> endorsedBy;

    public FabricEndorsedTransactionPayload(
            T_tx transaction,
            List<String> endorsedBy) {
        this.transaction = transaction;
        this.endorsedBy = endorsedBy;
    }

    @Override
    public int hashCode() {
        int result = this.transaction.hashCode();
        result = 31 * result + this.endorsedBy.hashCode();
        return result;
    }

}
