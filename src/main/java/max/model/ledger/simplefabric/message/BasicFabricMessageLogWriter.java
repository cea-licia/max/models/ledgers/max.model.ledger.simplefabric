/*********************************************************************
 * Copyright (c) 2023 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/

package max.model.ledger.simplefabric.message;


import madkit.kernel.AgentAddress;
import max.datatype.com.Message;
import max.model.ledger.simplefabric.message.payload.FabricEndorsedTransactionPayload;
import max.model.ledger.simplefabric.message.payload.FabricReplyEndorsementPayload;
import max.model.ledger.simplefabric.message.payload.FabricRequestEndorsementPayload;
import max.model.network.stochastic_adversarial_p2p.msglog.AbstractMessageLogWriter;


/**
 * So that any Fabric Peer, Orderer and Application may write into a log file any HyperLedger Fabric message event it observes locally i.e.,
 * the occurrences of the emission or the reception of a message on that node.
 * **/
public class BasicFabricMessageLogWriter<T_tx> implements AbstractMessageLogWriter {


    @Override
    public String messageToString(Message<AgentAddress, ?> message) {
        if (message.getPayload() instanceof FabricRequestEndorsementPayload<?>) {
            FabricRequestEndorsementPayload<T_tx> payload = (FabricRequestEndorsementPayload<T_tx>) message.getPayload();
            return String.format(
                    "HF_PROPOSE(%s,%s)",
                    payload.applicationName,
                    payload.transaction.toString());
        }
        if (message.getPayload() instanceof FabricReplyEndorsementPayload<?>) {
            FabricReplyEndorsementPayload<T_tx> payload = (FabricReplyEndorsementPayload<T_tx>) message.getPayload();
            return String.format(
                    "HF_ENDORSE(%s,%s)",
                    payload.transaction.toString(),
                    payload.endorsedBy);
        }
        if (message.getPayload() instanceof FabricEndorsedTransactionPayload<?>) {
            FabricEndorsedTransactionPayload<T_tx> payload = (FabricEndorsedTransactionPayload<T_tx>) message.getPayload();
            return String.format(
                    "HF_ORDER(%s,%s)",
                    payload.transaction,
                    payload.endorsedBy.toString());
        }
        return "";
    }

}
