/*********************************************************************
 * Copyright (c) 2023 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/

package max.model.ledger.simplefabric.message.handler;


import madkit.kernel.AgentAddress;
import max.datatype.com.Message;
import max.model.ledger.abstract_ledger.state.AbstractLocalLedgerState;
import max.model.ledger.simplefabric.action.comms.AcSendEndorsementReply;
import max.model.ledger.simplefabric.action.comms.AcSubmitEndorsedTransactionToOrdering;
import max.model.ledger.simplefabric.agent.FabricApplication;
import max.model.ledger.simplefabric.env.FabricContext;
import max.model.ledger.simplefabric.env.FabricEnvironment;
import max.model.ledger.simplefabric.message.payload.FabricReplyEndorsementPayload;
import max.model.ledger.simplefabric.message.payload.FabricRequestEndorsementPayload;
import max.model.network.stochastic_adversarial_p2p.agent.StochasticPeerAgent;
import max.model.network.stochastic_adversarial_p2p.context.StochasticP2PContext;
import max.model.network.stochastic_adversarial_p2p.context.handler.OnReceiveMessageHandler;

import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;


/**
 * Message handler that handles {@link max.model.ledger.simplefabric.message.FabricMessageType#ReplyWithEndorsement} messages.
 *
 * @author Erwan Mahe
 */
public class FabEndReplyHandler<T_tx, T_st extends AbstractLocalLedgerState<T_tx>> implements OnReceiveMessageHandler {

    @Override
    public void handle(StochasticP2PContext context, Message<AgentAddress, ?> message) {
        // ***
        FabricApplication<T_tx> fabApp = (FabricApplication<T_tx>) context.getOwner();

        FabricReplyEndorsementPayload<T_tx> fabPayload = (FabricReplyEndorsementPayload<T_tx> ) message.getPayload();

        String applicationName = fabApp.getName();
        if (fabApp.endorsementPolicy.register_endorsement(fabPayload.transaction, fabPayload.endorsedBy)) {
            fabApp.getLogger().info("Application " + applicationName + " acknowledges endorsement from peer " + fabPayload.endorsedBy);
            if (fabApp.endorsementPolicy.check_endorsement_threshold_reached(fabPayload.transaction)) {
                fabApp.getLogger().info("Threshold of endorsements reached, broadcasting endorsed transaction to ordering service");
                List<String> sortedEndorsedBy = fabApp.endorsementPolicy.get_endorsed_by(fabPayload.transaction);
                (new AcSubmitEndorsedTransactionToOrdering<>(
                        context.getEnvironmentName(),
                        fabApp,
                        fabPayload.transaction,
                        sortedEndorsedBy)
                ).execute();
            }
        } else {
            fabApp.getLogger().fine("Peer " + fabPayload.endorsedBy + " send unwanted (not anymore at least) endorsement to application " + applicationName);
        }



    }

}
