/*********************************************************************
 * Copyright (c) 2023 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/

package max.model.ledger.simplefabric.message.handler;


import madkit.kernel.AgentAddress;
import max.datatype.com.Message;
import max.model.ledger.abstract_ledger.action.transact.AcReceiveTransactionToPutInMempool;
import max.model.ledger.abstract_ledger.env.TransactionReceptionNature;
import max.model.ledger.abstract_ledger.state.AbstractLocalLedgerState;
import max.model.ledger.simplefabric.action.comms.AcSubmitEndorsedTransactionToOrdering;
import max.model.ledger.simplefabric.agent.FabricApplication;
import max.model.ledger.simplefabric.agent.FabricOrderer;
import max.model.ledger.simplefabric.env.FabricEnvironment;
import max.model.ledger.simplefabric.message.payload.FabricEndorsedTransactionPayload;
import max.model.ledger.simplefabric.message.payload.FabricReplyEndorsementPayload;
import max.model.ledger.simplefabric.role.RFabricOrderer;
import max.model.network.stochastic_adversarial_p2p.context.StochasticP2PContext;
import max.model.network.stochastic_adversarial_p2p.context.handler.OnReceiveMessageHandler;

import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;


/**
 * Message handler that handles {@link max.model.ledger.simplefabric.message.FabricMessageType#SubmitEndorsedTransaction} messages.
 *
 * @author Erwan Mahe
 */
public class FabSubmitToOrderingHandler<T_tx, T_st extends AbstractLocalLedgerState<T_tx>> implements OnReceiveMessageHandler {

    @Override
    public void handle(StochasticP2PContext context, Message<AgentAddress, ?> message) {
        // ***
        FabricOrderer fabOrd = (FabricOrderer) context.getOwner();

        FabricEndorsedTransactionPayload<T_tx> fabPayload = (FabricEndorsedTransactionPayload<T_tx> ) message.getPayload();

        String ordererName = fabOrd.getName();
        HashSet<String> endorsedByNoDuplicates = new HashSet<>(fabPayload.endorsedBy);
        if (fabOrd.endorsementPolicy.verify_endorsements_from_app(fabPayload.transaction,endorsedByNoDuplicates)) {
            fabOrd.getLogger().info("Ordered " + ordererName + " acknowledges transaction with enough endorsements");
            FabricEnvironment<T_tx,T_st> fabEnv = (FabricEnvironment<T_tx,T_st>) context.getEnvironment();
            (new AcReceiveTransactionToPutInMempool<T_tx,T_st,FabricOrderer>(
                    fabEnv.orderingServiceEnvironment.getName(),
                    fabOrd,
                    fabPayload.transaction,
                    TransactionReceptionNature.DIRECT_FROM_CLIENT)
            ).execute();
        } else {
            fabOrd.getLogger().fine("Not enough endorsements, refusing to acknowledge transaction");
        }

    }

}
