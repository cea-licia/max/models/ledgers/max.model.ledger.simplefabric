/*********************************************************************
 * Copyright (c) 2023 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/

package max.model.ledger.simplefabric.env;



import max.model.network.stochastic_adversarial_p2p.context.StochasticP2PContext;

import max.model.ledger.abstract_ledger.state.AbstractLocalLedgerState;
import max.model.network.stochastic_adversarial_p2p.agent.StochasticPeerAgent;



/**
 * The context of an agent that plays either RFabricApplication, RFabricPeer or RFabricOrderer role in a FabricEnvironment.
 *
 * @author Erwan Mahe
 */
public class FabricContext<T_tx, T_st extends AbstractLocalLedgerState<T_tx>> extends StochasticP2PContext {



    public FabricContext(
            StochasticPeerAgent owner,
            FabricEnvironment<T_tx,T_st> fabEnvironment) {
        super(owner, fabEnvironment);
    }


}

