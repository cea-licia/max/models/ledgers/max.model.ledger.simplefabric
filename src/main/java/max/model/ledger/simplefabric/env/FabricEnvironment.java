/*********************************************************************
 * Copyright (c) 2023 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/

package max.model.ledger.simplefabric.env;

import max.core.Context;
import max.core.agent.SimulatedAgent;
import max.model.ledger.abstract_ledger.env.AbstractLedgerEnvironment;
import max.model.ledger.abstract_ledger.state.AbstractLocalLedgerState;
import max.model.ledger.simplefabric.role.RFabricApplication;
import max.model.ledger.simplefabric.role.RFabricPeer;
import max.model.network.stochastic_adversarial_p2p.agent.StochasticPeerAgent;
import max.model.network.stochastic_adversarial_p2p.env.StochasticP2PEnvironment;

/**
 * Tendermint environment
 *
 * @author Erwan Mahe
 */
public class FabricEnvironment<T_tx, T_st extends AbstractLocalLedgerState<T_tx>> extends StochasticP2PEnvironment {



    public final AbstractLedgerEnvironment<T_tx,T_st> orderingServiceEnvironment;


    public FabricEnvironment(AbstractLedgerEnvironment<T_tx,T_st> orderingServiceEnvironment) {
        super();
        addAllowedRole(RFabricPeer.class);
        addAllowedRole(RFabricApplication.class);
        this.orderingServiceEnvironment = orderingServiceEnvironment;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected Context createContext(SimulatedAgent agent) {
        return new FabricContext<T_tx,T_st>((StochasticPeerAgent) agent, this);
    }

}
