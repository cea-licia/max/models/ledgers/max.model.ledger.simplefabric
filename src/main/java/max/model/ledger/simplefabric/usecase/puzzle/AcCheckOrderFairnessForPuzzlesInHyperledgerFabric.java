/*********************************************************************
 * Copyright (c) 2023 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/

package max.model.ledger.simplefabric.usecase.puzzle;


import madkit.kernel.AgentAddress;
import max.core.action.Action;
import max.core.role.Role;
import max.model.ledger.abstract_ledger.env.LedgerValidatorContext;
import max.model.ledger.abstract_ledger.role.RAbstractLedgerChannel;
import max.model.ledger.abstract_ledger.usecase.puzzle.PuzzleMockupLocalLedgerState;
import max.model.ledger.abstract_ledger.usecase.puzzle.PuzzleMockupTransaction;
import max.model.ledger.abstract_ledger.usecase.puzzle.action.AcCheckOrderFairnessForPuzzles;
import max.model.ledger.simplefabric.agent.FabricOrderer;
import max.model.ledger.simplefabric.agent.FabricPeer;
import max.model.ledger.simplefabric.env.FabricEnvironment;
import max.model.ledger.simplefabric.role.RFabricOrderer;
import max.model.ledger.simplefabric.role.RFabricPeer;
import max.model.network.stochastic_adversarial_p2p.agent.StochasticPeerAgent;
import max.model.network.stochastic_adversarial_p2p.env.StochasticP2PEnvironment;
import org.apache.commons.lang3.tuple.Pair;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;


/**
 * Action executed by the environment in which the ledger validators exist and validate transactions related to
 * the delivery of puzzle solutions in our puzzle usecase.
 *
 * Its aim is to verify the respect of three order-fairness properties in the ledger:
 * - receive-order fairness which dictates that if a majority of validators receive a transaction X before a transaction Y then X should be delivered before Y
 * - block/wave-order fairness which dictates that if a majority of validators receive a transaction X before a transaction Y then Y should not be delivered in a block that is before the block in which X is delivered
 * - differential-order fairness is a more refined form of receive-order fairness in which the difference between the number of times X is received before Y and of times Y is received before X is used
 *
 * This is a variant specific to HyperLedger Fabric which is particular because we have:
 * - the reception order on the peers
 * - the delivery order on the orderers
 *
 * Hence we cannot use the normal "AcCheckOrderFairnessForPuzzles" action in which
 * we suppose that both the reception and delivery orders are present in each node
 *
 * @author Erwan Mahe
 */
public class AcCheckOrderFairnessForPuzzlesInHyperledgerFabric extends Action<FabricEnvironment<PuzzleMockupTransaction,PuzzleMockupLocalLedgerState>> {

    private Optional<Pair<String,String>> outputCsvConf;

    private Integer receiveOrderThreshold;

    private Integer differentialOrderThreshold;

    private Class<? extends Role> validatorRoleInOrderingEnvironment;

    public AcCheckOrderFairnessForPuzzlesInHyperledgerFabric(String environment,
                                                             FabricEnvironment<PuzzleMockupTransaction,PuzzleMockupLocalLedgerState> owner,
                                                             Class<? extends Role> validatorRoleInOrderingEnvironment,
                                                             Integer receiveOrderThreshold,
                                                             Integer differentialOrderThreshold,
                                                             Optional<Pair<String,String>> outputCsvConf) {
        super(environment, RAbstractLedgerChannel.class, owner);
        this.validatorRoleInOrderingEnvironment = validatorRoleInOrderingEnvironment;
        this.receiveOrderThreshold = receiveOrderThreshold;
        this.differentialOrderThreshold = differentialOrderThreshold;
        this.outputCsvConf = outputCsvConf;
    }

    private PuzzleMockupLocalLedgerState getReferenceLocalLedgerState() {
        PuzzleMockupLocalLedgerState referenceLocalLedgerState = null;
        Integer maxNumKeys = 0;
        String referenceAgentName = null;
        String orderingServiceEnvironmentName = getOwner().orderingServiceEnvironment.getName();
        for (AgentAddress fabricOrdererAddress : this.getOwner().getAgentsWithRole(orderingServiceEnvironmentName, this.validatorRoleInOrderingEnvironment)) {
            FabricOrderer ordererAgent = (FabricOrderer) fabricOrdererAddress.getAgent();
            LedgerValidatorContext<PuzzleMockupTransaction,PuzzleMockupLocalLedgerState> context = (LedgerValidatorContext<PuzzleMockupTransaction,PuzzleMockupLocalLedgerState>) ordererAgent.getContext(orderingServiceEnvironmentName);
            // ***
            if (referenceAgentName == null) {
                referenceLocalLedgerState = context.ledgerValidatorState;
                referenceAgentName = ordererAgent.getName();
                maxNumKeys = referenceLocalLedgerState.deliveryOrderPerPuzzle.keySet().size();
            } else {
                if (context.ledgerValidatorState.deliveryOrderPerPuzzle.keySet().size() > maxNumKeys) {
                    referenceLocalLedgerState = context.ledgerValidatorState;
                    referenceAgentName = ordererAgent.getName();
                    maxNumKeys = referenceLocalLedgerState.deliveryOrderPerPuzzle.keySet().size();
                }
            }
        }
        this.getOwner().getLogger().info(
                "orderer " + referenceAgentName +
                        " has the most delivered puzzle solutions which is " + maxNumKeys +
                        "\n we will use it as a reference to check delivery order w.r.t. peers' local reception orders"
        );
        return referenceLocalLedgerState;
    }

    private HashMap<Pair<String,String>,Integer> collectNumberOfHappenBeforeReceptionsForPuzzleSolution(Integer puzzleIdentifier) {
        getOwner().getLogger().fine("checking pertinent order fairness properties for puzzle number " + puzzleIdentifier);
        HashMap<Pair<String,String>,Integer> numberOfTimesSolutionReceivedBeforeOther = new HashMap<>();
        // collecting happen-before relations on receptions of submitted solutions
        // on each (locally) validator
        for (AgentAddress fabricPeerAddress : this.getOwner().getAgentsWithRole(this.getEnvironment(), RFabricPeer.class)) {
            FabricPeer peerAgent = (FabricPeer) fabricPeerAddress.getAgent();
            PuzzleMockupApplicationSpecificPeerState peerState = (PuzzleMockupApplicationSpecificPeerState) peerAgent.applicationSpecificPeerState;
            // ***
            if (peerState.localReceiveOrderPerPuzzle.containsKey(puzzleIdentifier)) {
                List<String> localRecOrdForPuzzle = peerState.localReceiveOrderPerPuzzle.get(puzzleIdentifier);
                for (int i = 0; i < localRecOrdForPuzzle.size(); i++) {
                    String before = localRecOrdForPuzzle.get(i);
                    for (int j = i+1; j < localRecOrdForPuzzle.size(); j++) {
                        String after = localRecOrdForPuzzle.get(j);
                        getOwner().getLogger().fine(
                                "solution of puzzle " + puzzleIdentifier +
                                        " received from client " + before +
                                        " before client " + after +
                                        " on peer " + peerAgent.getName()
                        );
                        Pair<String,String> pair = Pair.of(before,after);
                        if (numberOfTimesSolutionReceivedBeforeOther.containsKey(pair)) {
                            Integer number = numberOfTimesSolutionReceivedBeforeOther.get(pair);
                            number = number + 1;
                            numberOfTimesSolutionReceivedBeforeOther.put(pair, number);
                        } else {
                            numberOfTimesSolutionReceivedBeforeOther.put(pair, 1);
                        }
                    }
                }
            }
        }
        // ***
        return numberOfTimesSolutionReceivedBeforeOther;
    }

    @Override
    public void execute() {
        PuzzleMockupLocalLedgerState referenceLocalLedgerState = this.getReferenceLocalLedgerState();
        List<Integer> sortedPuzzlesIdentifiers = referenceLocalLedgerState.deliveryOrderPerPuzzle.keySet().stream().sorted().collect(Collectors.toList());
        getOwner().getLogger().info("A total of " + sortedPuzzlesIdentifiers.size() + " puzzles have been solved");
        Integer numberOfTimesReceiveOrderFairnessViolated = 0;
        Integer numberOfTimesBlockOrderFairnessViolated = 0;
        Integer numberOfTimesDifferentialOrderFairnessViolated = 0;
        getOwner().getLogger().info("Checking several fairness properties related to these " + sortedPuzzlesIdentifiers.size() + " puzzle solving competitions");
        for (Integer puzzleIdentifier : sortedPuzzlesIdentifiers) {
            HashMap<Pair<String,String>,Integer> numberOfTimesSolutionReceivedBeforeOther = this.collectNumberOfHappenBeforeReceptionsForPuzzleSolution(puzzleIdentifier);
            // checking order fairness properties
            for (Pair<String,String> pair : numberOfTimesSolutionReceivedBeforeOther.keySet()) {
                Integer numberOfTimes = numberOfTimesSolutionReceivedBeforeOther.get(pair);
                String earlierSubmittingClient = pair.getLeft();
                String laterSubmittingClient = pair.getRight();
                if (referenceLocalLedgerState.deliveryOrderPerPuzzle.containsKey(puzzleIdentifier)) {
                    // collects delivery order
                    List<Pair<String,Integer>> deliveryOrderForPuzzle = referenceLocalLedgerState.deliveryOrderPerPuzzle.get(puzzleIdentifier);
                    Integer earlierIndex = null;
                    Integer earlierBlockNum = null;
                    Integer laterIndex = null;
                    Integer laterBlockNum = null;
                    for (int i = 0; i < deliveryOrderForPuzzle.size(); i++) {
                        Pair<String,Integer> got = deliveryOrderForPuzzle.get(i);
                        if (got.getLeft().equals(earlierSubmittingClient)) {
                            earlierIndex = i;
                            earlierBlockNum = got.getRight();
                        }
                        if (got.getLeft().equals(laterSubmittingClient)) {
                            laterIndex = i;
                            laterBlockNum = got.getRight();
                        }
                    }

                    // if both received transactions are indeed delivered
                    // then we can check order-fairness
                    if (earlierIndex != null && laterIndex != null) {
                        if (numberOfTimes >= this.receiveOrderThreshold) {
                            // receive-order fairness is violated
                            // if the earlier transaction is delivered
                            // after the later transaction is delivered
                            if (earlierIndex > laterIndex) {
                                numberOfTimesReceiveOrderFairnessViolated += 1;
                            }
                            // block-order fairness is violated
                            // if the earlier transaction is delivered in a block
                            // that is after the block in which
                            // the later transaction is delivered
                            if (earlierBlockNum > laterBlockNum) {
                                numberOfTimesBlockOrderFairnessViolated += 1;
                            }
                        }
                        // for differential order fairness we must substract
                        // from the number of times X is received before Y
                        // the number of times Y is received before X
                        Pair<String,String> inverted_pair = Pair.of(laterSubmittingClient,earlierSubmittingClient);
                        if (numberOfTimesSolutionReceivedBeforeOther.containsKey(inverted_pair)) {
                            Integer difference = numberOfTimes - numberOfTimesSolutionReceivedBeforeOther.get(inverted_pair);
                            if (difference >= this.differentialOrderThreshold) {
                                // if the difference between the number of times X is received before Y
                                // minus the number of times Y is received before X
                                // is greater than the (2*f) threshold
                                // then X must be delivered before Y
                                // if this is not the case differential-order fairness is violated
                                if (earlierIndex > laterIndex) {
                                    numberOfTimesDifferentialOrderFairnessViolated += 1;
                                }
                            }
                        }
                    }
                } else {
                    getOwner().getLogger().warning("missing delivery order for puzzle " + puzzleIdentifier);
                }
            }
        }
        // ***
        List<String> csvLine = new ArrayList<>();
        // collect client-fairness i.e., in this usecase,
        // the likelihood of each client to win puzzles
        // (over the repeated instances during the simulation)
        // this is summarized via a score given to each client
        // this scores corresponds to : %w * n
        // where %w corresponds to the percentage of WON puzzles for a given client
        // and n the total number of clients
        // if the simulation is "client-fair" then every client has 1/n changes of winning every puzzle
        // hence, the simulation is "client-fair" if the scores of all clients converge towards 1
        Integer numberOfPuzzlesSolved = sortedPuzzlesIdentifiers.size();
        if (numberOfPuzzlesSolved > 0) {
            // then write
            getOwner().getLogger().info("receive-order      fairness for puzzle solutions w.r.t. HF endorsing service has been violated : " + String.valueOf(numberOfTimesReceiveOrderFairnessViolated) + " times");
            csvLine.add( String.valueOf(numberOfTimesReceiveOrderFairnessViolated) );
            getOwner().getLogger().info("block/wave-order   fairness for puzzle solutions w.r.t. HF endorsing service has been violated : " + String.valueOf(numberOfTimesBlockOrderFairnessViolated) + " times");
            csvLine.add( String.valueOf(numberOfTimesBlockOrderFairnessViolated) );
            getOwner().getLogger().info("differential-order fairness for puzzle solutions w.r.t. HF endorsing service has been violated : " + String.valueOf(numberOfTimesDifferentialOrderFairnessViolated) + " times");
            csvLine.add( String.valueOf(numberOfTimesDifferentialOrderFairnessViolated) );
            // ***
            // Print results as a line in a CSV file
            if (this.outputCsvConf.isPresent()) {
                Pair<String,String> csvConf = this.outputCsvConf.get();
                String outputCsvFilePath = csvConf.getLeft();
                String csvLinePrefix = csvConf.getRight();
                try {
                    FileWriter fw = new FileWriter(outputCsvFilePath, false);
                    BufferedWriter bw = new BufferedWriter(fw);
                    bw.write( csvLinePrefix + String.join(";", csvLine) );
                    bw.newLine();
                    bw.close();
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
        }

    }

    @Override
    public <T extends Action<FabricEnvironment<PuzzleMockupTransaction,PuzzleMockupLocalLedgerState>>> T copy() {
        return (T) new AcCheckOrderFairnessForPuzzlesInHyperledgerFabric(
                getEnvironment(),
                getOwner(),
                this.validatorRoleInOrderingEnvironment,
                this.receiveOrderThreshold,
                this.differentialOrderThreshold,
                this.outputCsvConf);
    }

}
