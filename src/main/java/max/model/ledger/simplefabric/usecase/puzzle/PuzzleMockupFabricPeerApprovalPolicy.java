/*********************************************************************
 * Copyright (c) 2023 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/

package max.model.ledger.simplefabric.usecase.puzzle;


import max.model.ledger.abstract_ledger.usecase.puzzle.PuzzleMockupTransaction;
import max.model.ledger.simplefabric.agent.peer_approval.AbstractFabricPeerApprovalPolicy;

import java.util.HashSet;

/**
 * Approve puzzle solutions if not from a set of ostracized clients.
 *
 * @author Erwan Mahe
 */
public class PuzzleMockupFabricPeerApprovalPolicy implements AbstractFabricPeerApprovalPolicy<PuzzleMockupTransaction> {

    private final HashSet<String> ostracizedClients;

    public PuzzleMockupFabricPeerApprovalPolicy(HashSet<String> ostracizedClients) {
        this.ostracizedClients = ostracizedClients;
    }

    @Override
    public boolean approve(PuzzleMockupTransaction transactionToApprove) {
        if (transactionToApprove.clientName.isPresent()) {
            String clientName = transactionToApprove.clientName.get();
            return !(this.ostracizedClients.contains(clientName));
        } else {
            return true;
        }
    }

    @Override
    public AbstractFabricPeerApprovalPolicy<PuzzleMockupTransaction> copy_as_new() {
        return new PuzzleMockupFabricPeerApprovalPolicy(new HashSet<>(this.ostracizedClients));
    }

}
