/*********************************************************************
 * Copyright (c) 2023 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/

package max.model.ledger.simplefabric.usecase.puzzle;


import max.model.ledger.abstract_ledger.usecase.puzzle.PuzzleMockupTransaction;
import max.model.ledger.simplefabric.peer_state.ApplicationSpecificPeerState;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * ApplicationSpecificPeerState that is specific to our puzzle mockup application layer.
 * This allows HyperLedger Fabric peers to keep track of the reception order of puzzle solutions from clients
 * when received from an application at the step when the applications collect endorsements.
 *
 * @author Erwan Mahe
 */
public class PuzzleMockupApplicationSpecificPeerState implements ApplicationSpecificPeerState<PuzzleMockupTransaction> {

    // the order with which the validator received solutions submitted by clients for each puzzle
    public HashMap<Integer, List<String>> localReceiveOrderPerPuzzle;

    /**
     * Default constructor.
     *
     * We initialize the transaction storage as empty.
     */
    public PuzzleMockupApplicationSpecificPeerState() {
        this.localReceiveOrderPerPuzzle = new HashMap<>();
    }


    @Override
    public void onReceiveEndorsementRequest(PuzzleMockupTransaction transaction) {
        if (transaction.clientName.isPresent()) {
            String clientName = transaction.clientName.get();
            if (this.localReceiveOrderPerPuzzle.containsKey(transaction.puzzleIdentifier)) {
                List<String> orderOnPuzzle = this.localReceiveOrderPerPuzzle.get(transaction.puzzleIdentifier);
                if (!orderOnPuzzle.contains(clientName)) {
                    orderOnPuzzle.add(clientName);
                    this.localReceiveOrderPerPuzzle.put(transaction.puzzleIdentifier, orderOnPuzzle);
                }
            } else {
                List<String> orderOnPuzzle = new ArrayList<>();
                orderOnPuzzle.add(clientName);
                this.localReceiveOrderPerPuzzle.put(transaction.puzzleIdentifier, orderOnPuzzle);
            }
        }
    }

    @Override
    public ApplicationSpecificPeerState<PuzzleMockupTransaction> copy_as_new() {
        return new PuzzleMockupApplicationSpecificPeerState();
    }

}
