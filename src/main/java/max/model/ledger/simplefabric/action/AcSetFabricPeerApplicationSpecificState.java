/*********************************************************************
 * Copyright (c) 2023 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/

package max.model.ledger.simplefabric.action;


import max.core.action.Action;
import max.model.ledger.abstract_ledger.state.AbstractLocalLedgerState;
import max.model.ledger.simplefabric.agent.FabricPeer;
import max.model.ledger.simplefabric.peer_state.ApplicationSpecificPeerState;
import max.model.ledger.simplefabric.role.RFabricPeer;


/**
 * Action executed to (re)set the application specific state of a HF Peer.
 *
 * @author Erwan Mahe
 */
public class AcSetFabricPeerApplicationSpecificState<T_tx, T_st extends AbstractLocalLedgerState<T_tx>> extends Action<FabricPeer<T_tx>> {

    private final ApplicationSpecificPeerState<T_tx> applicationSpecificPeerState;

    public AcSetFabricPeerApplicationSpecificState(String environmentName,
                                                   FabricPeer<T_tx> owner,
                                                   ApplicationSpecificPeerState<T_tx> applicationSpecificPeerState) {
        super(environmentName, RFabricPeer.class, owner);
        this.applicationSpecificPeerState = applicationSpecificPeerState;
    }

    @Override
    public void execute() {
        getOwner().getLogger().info("modifying the application specific state of peer " + getOwner().getName());
        getOwner().applicationSpecificPeerState = this.applicationSpecificPeerState;
    }

    @Override
    public <T extends Action<FabricPeer<T_tx>>> T copy() {
        return (T) new AcSetFabricPeerApplicationSpecificState(
                getEnvironment(),
                getOwner(),
                this.applicationSpecificPeerState.copy_as_new());
    }
}
