/*********************************************************************
 * Copyright (c) 2023 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/

package max.model.ledger.simplefabric.action.comms;

import madkit.kernel.AgentAddress;
import max.core.action.Action;
import max.datatype.com.Message;
import max.model.ledger.abstract_ledger.state.AbstractLocalLedgerState;
import max.model.ledger.simplefabric.agent.FabricApplication;
import max.model.ledger.simplefabric.message.FabricMessageType;
import max.model.ledger.simplefabric.message.payload.FabricRequestEndorsementPayload;
import max.model.ledger.simplefabric.role.RFabricApplication;
import max.model.ledger.simplefabric.role.RFabricPeer;
import max.model.network.stochastic_adversarial_p2p.action.message.AcEmitMessageTowardsNetwork;
import max.model.network.stochastic_adversarial_p2p.agent.StochasticPeerAgent;

import java.util.*;


/**
 * Action executed when an HF application broadcasts to all the peers of the endorsing service
 * a transaction so that the peers may endorse it.
 *
 * @author Erwan Mahe
 */
public class AcSubmitEndorsementRequestToEndorsing<T_tx, T_st extends AbstractLocalLedgerState<T_tx>, A extends StochasticPeerAgent> extends Action<A> {


    private final T_tx transaction;


    public AcSubmitEndorsementRequestToEndorsing(String drEnvironmentName,
                                                 A owner,
                                                 T_tx transaction) {
        super(drEnvironmentName, RFabricApplication.class, owner);
        this.transaction = transaction;
    }

    @Override
    public void execute() {
        FabricApplication<T_tx> fabApp = (FabricApplication<T_tx>) getOwner();
        AgentAddress myAddress = this.getOwner().getContext(this.getEnvironment()).getMyAddress(RFabricApplication.class.getName());
        String applicationName = fabApp.getName();
        // ***
        // retrieve receivers information
        Set<String> receiversNames = new HashSet<>();
        List<AgentAddress> receivers = new ArrayList<>();
        for (AgentAddress agentAddress : this.getOwner().getAgentsWithRole(this.getEnvironment(), RFabricPeer.class)) {
            var agentName = agentAddress.getAgent().getName();
            receiversNames.add(agentName);
            receivers.add(agentAddress);
        }
        // ***
        // create payload and message
        FabricRequestEndorsementPayload<T_tx> payload = new FabricRequestEndorsementPayload<T_tx>(
                this.transaction,
                applicationName);
        Message<AgentAddress, FabricRequestEndorsementPayload<T_tx>> message = new Message<>(
                myAddress,
                receivers,
                payload);
        message.setType(FabricMessageType.RequestEndorsement.name());
        message.setCountKey(FabricMessageType.RequestEndorsement.name());
        // ***
        getOwner().getLogger().info(
                "Application " + applicationName +
                        " broadcast endorsement request to peers : " +
                        receiversNames
        );
        // ***
        (new AcEmitMessageTowardsNetwork<StochasticPeerAgent>(
                this.getEnvironment(),
                (StochasticPeerAgent) this.getOwner(),
                message)
        ).execute();
        // ***
        // keeps track of endorsements for this transaction
        fabApp.endorsementPolicy.initiate_transaction(this.transaction);
    }

    @Override
    public <T extends Action<A>> T copy() {
        return (T) new AcSubmitEndorsementRequestToEndorsing(
                getEnvironment(),
                getOwner(),
                this.transaction);
    }
}
