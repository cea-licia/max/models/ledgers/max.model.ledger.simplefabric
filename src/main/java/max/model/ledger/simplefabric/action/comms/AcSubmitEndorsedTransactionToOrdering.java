/*********************************************************************
 * Copyright (c) 2023 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/

package max.model.ledger.simplefabric.action.comms;

import madkit.kernel.AgentAddress;
import max.core.action.Action;
import max.datatype.com.Message;
import max.model.ledger.simplefabric.agent.FabricApplication;
import max.model.ledger.simplefabric.message.FabricMessageType;
import max.model.ledger.simplefabric.message.payload.FabricEndorsedTransactionPayload;
import max.model.ledger.simplefabric.message.payload.FabricReplyEndorsementPayload;
import max.model.ledger.simplefabric.role.RFabricApplication;
import max.model.ledger.simplefabric.role.RFabricOrderer;
import max.model.ledger.simplefabric.role.RFabricPeer;
import max.model.network.stochastic_adversarial_p2p.action.message.AcEmitMessageTowardsNetwork;
import max.model.network.stochastic_adversarial_p2p.agent.StochasticPeerAgent;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


/**
 * Action executed when a Hyperledger Fabric application broadcasts an endorsed transaction to HF orderers.
 *
 * @author Erwan Mahe
 */
public class AcSubmitEndorsedTransactionToOrdering<T_tx, A extends FabricApplication> extends Action<A> {


    private final T_tx transaction;

    private final List<String> endorsedBy;


    public AcSubmitEndorsedTransactionToOrdering(String drEnvironmentName,
                                                 A owner,
                                                 T_tx transaction,
                                                 List<String> endorsedBy) {
        super(drEnvironmentName, RFabricApplication.class, owner);
        this.transaction = transaction;
        this.endorsedBy = endorsedBy;
    }

    @Override
    public void execute() {
        // ***
        // retrieve sender information
        AgentAddress myAddress = this.getOwner().getContext(this.getEnvironment()).getMyAddress(RFabricApplication.class.getName());
        // ***
        // retrieve receivers information : receivers are those playing RFabricOrderer
        List<String> receiversNames = new ArrayList<>();
        List<AgentAddress> receivers = new ArrayList<>();
        for (AgentAddress agentAddress : this.getOwner().getAgentsWithRole(this.getEnvironment(), RFabricOrderer.class)) {
            var agentName = agentAddress.getAgent().getName();
            receiversNames.add(agentName);
            receivers.add(agentAddress);
        }
        // ***
        // create payload and message
        FabricEndorsedTransactionPayload<T_tx> payload = new FabricEndorsedTransactionPayload<T_tx>(
                this.transaction,
                this.endorsedBy);
        Message<AgentAddress, FabricEndorsedTransactionPayload> message = new Message<>(
                myAddress,
                receivers,
                payload);
        message.setType(FabricMessageType.SubmitEndorsedTransaction.name());
        message.setCountKey(FabricMessageType.SubmitEndorsedTransaction.name());
        // ***
        getOwner().getLogger().info(
                "Application " + getOwner().getName() +
                        " broadcasting transaction\n - endorsed by the peers " + this.endorsedBy  + "\n - to the orderers " +
                        receiversNames
        );
        // ***
        (new AcEmitMessageTowardsNetwork<StochasticPeerAgent>(
                this.getEnvironment(),
                (StochasticPeerAgent) this.getOwner(),
                message)
        ).execute();
        // ***
        FabricApplication<T_tx> fabApp = (FabricApplication<T_tx>) this.getOwner();
        fabApp.endorsementPolicy.forget_transaction(this.transaction);
    }

    @Override
    public <T extends Action<A>> T copy() {
        return (T) new AcSubmitEndorsedTransactionToOrdering(
                getEnvironment(),
                getOwner(),
                this.transaction,
                this.endorsedBy);
    }
}
