/*********************************************************************
 * Copyright (c) 2023 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/

package max.model.ledger.simplefabric.action.comms;

import madkit.kernel.AgentAddress;
import max.core.action.Action;
import max.datatype.com.Message;
import max.model.ledger.simplefabric.message.FabricMessageType;
import max.model.ledger.simplefabric.message.payload.FabricReplyEndorsementPayload;
import max.model.ledger.simplefabric.role.RFabricApplication;
import max.model.ledger.simplefabric.role.RFabricPeer;
import max.model.network.stochastic_adversarial_p2p.action.message.AcEmitMessageTowardsNetwork;
import max.model.network.stochastic_adversarial_p2p.agent.StochasticPeerAgent;


import java.util.*;


/**
 * Action executed when an HF peer sends to a client a response in which the peer endorses the transaction
 * the client has previously submitted.
 *
 * @author Erwan Mahe
 */
public class AcSendEndorsementReply<T_tx, A extends StochasticPeerAgent> extends Action<A> {


    private final T_tx transaction;

    private final String applicationName;


    public AcSendEndorsementReply(String drEnvironmentName,
                                  A owner,
                                  T_tx transaction,
                                  String applicationName) {
        super(drEnvironmentName, RFabricPeer.class, owner);
        this.transaction = transaction;
        this.applicationName = applicationName;
    }

    @Override
    public void execute() {
        // ***
        // retrieve sender information
        AgentAddress myAddress = this.getOwner().getContext(this.getEnvironment()).getMyAddress(RFabricPeer.class.getName());
        String endorsingPeerName = myAddress.getAgent().getName();
        // ***
        // retrieve receivers information
        boolean found = false;
        List<AgentAddress> receivers = new ArrayList<>();
        for (AgentAddress agentAddress : this.getOwner().getAgentsWithRole(this.getEnvironment(), RFabricApplication.class)) {
            var agentName = agentAddress.getAgent().getName();
            if (agentName.equals(this.applicationName)) {
                receivers.add(agentAddress);
                found = true;
                break;
            }
        }
        if (!found) {
            getOwner().getLogger().warning(
                    "Peer " + endorsingPeerName +
                            " could not find application " +
                            this.applicationName +
                            " to which send an endorsement"
            );
            return;
        }
        // ***
        // create payload and message
        FabricReplyEndorsementPayload payload = new FabricReplyEndorsementPayload(
                this.transaction,
                endorsingPeerName);
        Message<AgentAddress, FabricReplyEndorsementPayload> message = new Message<>(
                myAddress,
                receivers,
                payload);
        message.setType(FabricMessageType.ReplyWithEndorsement.name());
        message.setCountKey(FabricMessageType.ReplyWithEndorsement.name());
        // ***
        getOwner().getLogger().info(
                "Peer " + endorsingPeerName +
                        " sending endorsement to application " +
                        this.applicationName
        );
        // ***
        (new AcEmitMessageTowardsNetwork<StochasticPeerAgent>(
                this.getEnvironment(),
                (StochasticPeerAgent) this.getOwner(),
                message)
        ).execute();
    }

    @Override
    public <T extends Action<A>> T copy() {
        return (T) new AcSendEndorsementReply(
                getEnvironment(),
                getOwner(),
                this.transaction,
                this.applicationName);
    }
}
