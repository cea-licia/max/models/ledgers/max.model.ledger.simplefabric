/*********************************************************************
 * Copyright (c) 2023 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/

package max.model.ledger.simplefabric.peer_state;


/**
 * Application specific state for Hyperledger Fabric peers.
 * So as to be able to change peers' internal state upon receiving a transaction.
 * We keep it generic so that behavior that is specific to an application layer can be implemented.
 *
 * @author Erwan Mahe
 */
public interface ApplicationSpecificPeerState<T_tx> {

    void onReceiveEndorsementRequest(T_tx transaction);

    ApplicationSpecificPeerState<T_tx> copy_as_new();

}
