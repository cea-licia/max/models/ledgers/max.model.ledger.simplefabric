/*********************************************************************
 * Copyright (c) 2023 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/

package max.model.ledger.simplefabric.peer_state;


/**
 * Default ApplicationSpecificPeerState that does nothing.
 *
 * @author Erwan Mahe
 */
public class DefaultApplicationSpecificPeerState<T_tx> implements ApplicationSpecificPeerState<T_tx> {

    @Override
    public void onReceiveEndorsementRequest(T_tx transaction) {
        // do nothing
    }

    @Override
    public ApplicationSpecificPeerState<T_tx> copy_as_new() {
        return new DefaultApplicationSpecificPeerState();
    }

}
