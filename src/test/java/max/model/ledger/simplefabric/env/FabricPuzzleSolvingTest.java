/*********************************************************************
 * Copyright (c) 2023 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/

package max.model.ledger.simplefabric.env;

import max.core.action.ACTakeRole;
import max.core.role.RExperimenter;
import max.model.ledger.abstract_ledger.action.check.AcCheckCoherence;
import max.model.ledger.abstract_ledger.action.check.AcCollectDeliveredTransactionsMetrics;
import max.model.ledger.abstract_ledger.usecase.puzzle.PuzzleMockupTransaction;
import max.model.ledger.abstract_ledger.usecase.puzzle.PuzzleMockupTransactionGenerator;
import max.model.ledger.abstract_ledger.usecase.puzzle.action.AcCheckOrderFairnessForPuzzles;
import max.model.ledger.simplefabric.action.AcNotifyClientsPuzzleSolutionToFabricApp;
import max.model.ledger.simplefabric.agent.FabricApplication;
import max.model.ledger.simplefabric.agent.FabricOrderer;
import max.model.ledger.simplefabric.agent.FabricPeer;
import max.model.ledger.simplefabric.agent.endorsement_policy.AbstractFabricEndorsementPolicy;
import max.model.ledger.simplefabric.agent.endorsement_policy.OrganisationCentricFabricEndorsementPolicy;
import max.model.ledger.simplefabric.agent.peer_approval.AcceptingFabricPeerApprovalPolicy;
import max.model.ledger.simplefabric.exp.FabricExperimenter;
import max.model.ledger.simplefabric.role.RFabricChannel;
import max.model.ledger.simplefabric.usecase.puzzle.AcCheckOrderFairnessForPuzzlesInHyperledgerFabric;
import max.model.ledger.simplefabric.usecase.puzzle.PuzzleMockupApplicationSpecificPeerState;
import max.model.ledger.simplemint.behavior.TmProposerSelector;
import max.model.ledger.simplemint.data_types.hasher.TmHasherSingleton;
import max.model.ledger.simplemint.role.RTendermintChannel;
import max.model.ledger.simplemint.role.RTendermintValidator;
import max.model.network.stochastic_adversarial_p2p.action.AcCollectMessageCount;
import max.model.network.stochastic_adversarial_p2p.context.delay.ContinuousUniformDelay;
import max.model.network.stochastic_adversarial_p2p.context.delay.DelaySpecification;
import max.model.network.stochastic_adversarial_p2p.msgcount.MessageCounterSingleton;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.TestInfo;
import org.junit.jupiter.api.io.TempDir;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import java.math.BigDecimal;
import java.nio.file.Path;
import java.util.*;
import java.util.logging.Level;

import static max.core.MAXParameters.*;
import static max.core.test.TestMain.launchTester;

/**
 * Basic Test in which we set up a Fabric network
 *
 * @author Erwan Mahe
 */
public class FabricPuzzleSolvingTest {

    @BeforeEach
    public void before(@TempDir Path tempDir) {
        clearParameters();

        PuzzleMockupTransactionGenerator.resetGenerator();
        TmHasherSingleton.resetHasher();
        MessageCounterSingleton.resetCounter();

        setParameter("MAX_CORE_SIMULATION_STEP", BigDecimal.ONE.toString());
        setParameter("MAX_CORE_UI_MODE", "Silent");
        setParameter("MAX_CORE_RESULTS_FOLDER_NAME", tempDir.toString());
    }


    @ParameterizedTest
    @CsvSource({
            "2,2,2,2"
            //,"10,15,5"
    })
    public void test(
            int ordByzNum,
            int peersPerOrg,
            int numberOfOrganizations,
            int clientCount,
            TestInfo testInfo)
            throws Throwable {

        boolean printLogs = false;


        List<String> clientsList = new ArrayList<>();
        for (int i=0;i< clientCount;i++) {
            clientsList.add("client" + String.valueOf(i));
        }

        HashMap<String,String> peerToOrganizationMap = new HashMap<>();
        HashMap<String,Integer> thresholdPerOrganization = new HashMap<>();
        for (int i=0;i< numberOfOrganizations;i++) {
            String orgName = "org" + String.valueOf(i);
            thresholdPerOrganization.put(orgName, peersPerOrg/2);
            for (int j=0;j<peersPerOrg;j++) {
                String peerName = orgName + "peer" + String.valueOf(j);
                peerToOrganizationMap.put(peerName,orgName);
            }
        }

        int ordCount = 3 * ordByzNum + 1;

        boolean silent = false;


        // Tendermint phase timeout
        BigDecimal tendermintPhaseTimeoutDuration = BigDecimal.valueOf(25);

        // rate of new puzzles
        BigDecimal new_puzzles_rate = BigDecimal.valueOf(10);

        // duration for solving a puzzle
        Optional<DelaySpecification> delayBetweenClientsAndApp = Optional.of(
                new ContinuousUniformDelay(1, 10)
        );

        Optional<DelaySpecification> baseline_output_delay = Optional.of(
                new ContinuousUniformDelay(5, 10)
        );
        Optional<DelaySpecification> baseline_input_delay = Optional.of(
                new ContinuousUniformDelay(5, 10)
        );

        // Test duration in ticks
        final var testDuration = 1000;

        // Setup tester
        final var tester =
                new FabricExperimenter() {

                    @Override
                    protected void setupApplications() {
                        final var appPlan = buildApplicationPlan(baseline_output_delay,baseline_input_delay,printLogs);
                        AbstractFabricEndorsementPolicy<PuzzleMockupTransaction> policy = new OrganisationCentricFabricEndorsementPolicy(
                                new HashMap<>(peerToOrganizationMap),
                                new HashMap<>(thresholdPerOrganization)
                        );
                        FabricApplication<PuzzleMockupTransaction> app = new FabricApplication<PuzzleMockupTransaction>(appPlan, policy);
                        if (silent) {
                            app.getLogger().setLevel(Level.WARNING);
                        }
                        this.addApplication(app);
                    }

                    @Override
                    protected void setupPeers() {
                        for (String peerName : peerToOrganizationMap.keySet()) {
                            FabricPeer<PuzzleMockupTransaction> peer = new FabricPeer<PuzzleMockupTransaction>(
                                    buildPeerPlan(
                                            baseline_output_delay,
                                            baseline_input_delay,
                                            printLogs
                                    ),
                                    new AcceptingFabricPeerApprovalPolicy<>(),
                                    new PuzzleMockupApplicationSpecificPeerState()
                            );
                            peer.setName(peerName);
                            if (silent) {
                                peer.getLogger().setLevel(Level.WARNING);
                            }
                            this.addPeer(peer);
                        }
                    }

                    @Override
                    protected void setupOrderers() {
                        for (var i = 0;
                             i < ordCount;
                             ++i) {
                            String ordererName = "orderer" + String.valueOf(i);
                            AbstractFabricEndorsementPolicy<PuzzleMockupTransaction> policy = new OrganisationCentricFabricEndorsementPolicy(
                                    new HashMap<>(peerToOrganizationMap),
                                    new HashMap<>(thresholdPerOrganization)
                            );
                            FabricOrderer orderer = new FabricOrderer(
                                    buildOrdererPlan(
                                            baseline_output_delay,
                                            baseline_input_delay,
                                            ordByzNum,
                                            tendermintPhaseTimeoutDuration,
                                            printLogs),
                                    policy
                            );
                            orderer.setName(ordererName);
                            orderer.addLedgerValidatorRole(RTendermintValidator.class);
                            if (silent) {
                                orderer.getLogger().setLevel(Level.WARNING);
                            }
                            this.addOrderer(orderer);
                        }
                        TmProposerSelector.setInstance(this.getOrderersNames());
                    }


                    @Override
                    protected void setupExperimenter() {
                        schedule(
                                new ACTakeRole<>(this.fabEnvironment.getName(),
                                        RExperimenter.class,
                                        this.fabEnvironment)
                                        .oneTime(BigDecimal.ZERO)
                        );
                        schedule(
                                new ACTakeRole<>(this.fabEnvironment.getName(),
                                        RFabricChannel.class,
                                        this.fabEnvironment)
                                        .oneTime(BigDecimal.ZERO)
                        );
                        schedule(
                                new ACTakeRole<>(this.fabEnvironment.orderingServiceEnvironment.getName(),
                                        RTendermintChannel.class,
                                        this.fabEnvironment.orderingServiceEnvironment)
                                        .oneTime(BigDecimal.ZERO)
                        );

                        schedule(
                                new AcNotifyClientsPuzzleSolutionToFabricApp(
                                        this.fabEnvironment.getName(),
                                        this.fabEnvironment,
                                        clientsList,
                                        delayBetweenClientsAndApp,
                                        new HashMap<>())
                                        .repeatFinitely(BigDecimal.valueOf(3),BigDecimal.valueOf(testDuration - 10),new_puzzles_rate)
                        );

                        //String csvFilePath = "C:\\Users\\em244186\\idea_projects\\simplemint\\" + "puzzle_" + String.valueOf(nodeCount) + "nodes_" + String.valueOf(clientCount) + "clients.csv";
                        //String prefix = String.join(";", clientsList) + ";rec_ord_fair;blo_ord_fair;dif_ord_fair\n";

                        schedule(
                                new AcCheckCoherence<>(
                                        this.fabEnvironment.orderingServiceEnvironment.getName(),
                                        this.fabEnvironment.orderingServiceEnvironment,
                                        RTendermintValidator.class)
                                        .oneTime(testDuration)
                        );
                        schedule(
                                new AcCollectDeliveredTransactionsMetrics<>(
                                        this.fabEnvironment.orderingServiceEnvironment.getName(),
                                        this.fabEnvironment.orderingServiceEnvironment,
                                        RTendermintValidator.class,
                                        Optional.empty())
                                        .oneTime(testDuration)
                        );
                        schedule(
                                new AcCollectMessageCount(
                                        this.fabEnvironment.getName(),
                                        this.fabEnvironment,
                                        Optional.empty())
                                        .oneTime(testDuration)
                        );
                        schedule(
                                new AcCheckOrderFairnessForPuzzles(
                                        this.fabEnvironment.orderingServiceEnvironment.getName(),
                                        this.fabEnvironment.orderingServiceEnvironment,
                                        RTendermintValidator.class,
                                        ordCount / 2,
                                        2 * ordByzNum,
                                        clientsList,
                                        clientCount,
                                        Optional.empty())
                                        .oneTime(testDuration)
                        );
                        int peerCount = peersPerOrg*numberOfOrganizations;
                        schedule(
                                new AcCheckOrderFairnessForPuzzlesInHyperledgerFabric(
                                        this.fabEnvironment.getName(),
                                        this.fabEnvironment,
                                        RTendermintValidator.class,
                                        peerCount / 2,
                                        2 * peerCount / 3,
                                        Optional.empty())
                                        .oneTime(testDuration)
                        );


                    }
                };
        launchTester(tester, testDuration, testInfo);
    }
}
